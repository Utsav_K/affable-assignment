describe("Testing API EndPoints For User Registration and Login Flow", () => {
  

  let username = "user1";
  let email = "user1@email.com";
  let password = "123456";
  let token;
  let invalid_email = "invalid@email.com"
  it("Test Register (POST) Request for user registration", () => {
    cy.request({
        method : 'POST',
        url: "http://localhost:3333/api/users",
        body: {
            "username": username,
            "email" : email,
            "password" : password
        }
    })
         .then((response) => {
             if (response.body.statusCode != 200){}
                expect(response.body).to.have.property('statusCode', 200);
                expect(response.body.message).to.equal("Registered successfully.");
           
    })
})

it("Test Login (POST) Request for valid email and password", () => {
    cy.request({
        method : 'POST',
        url: "http://localhost:3333/api/users/login",
        body: {
            "email" : email,
            "password" :password
        }
    })
         .then((response) => {

            if (response.body.statusCode != 200){}
            expect(response.body).to.have.property('statusCode', 200);
            expect(response.body.message).to.equal("Logged in successfully");
        
    })
})

it("Test Login (POST) Request for invalid email or password", () => {
    cy.request({
        method : 'POST',
        url: "http://localhost:3333/api/users/login",
        body: {
            "email" : invalid_email,
            "password" :password
        },
        failOnStatusCode: false
    })
    
         .then((response) => {
            expect(response.body).to.have.property('statusCode', 404);
            expect(response.body.message).to.equal("Not found");
    })
})
 
})