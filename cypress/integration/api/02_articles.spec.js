
let token; // User's Token for Authorization
let slug; // Used in the url to update/delete the Posts
describe("Testing API EndPoints For Posts Created, Updated and Deleted", () => {
    
    it("Login to Get Token", () => {
        cy.request({
            method : 'POST',
            url: "http://localhost:3333/api/users/login",
            body: {
                "email" : "test@email.com",
                "password" :"123456"
            }
        })
             .then((response) => {
                    expect(response.body).to.have.property('statusCode', 200);
                    expect(response.body.data).to.have.property('token');
                    token = response.body.data.token;
                    cy.log(token)
        })
    })

    
    it("Test Post Articles Request", () => {
        const authorization = 'Bearer ' + token;
        
          cy.request({
              method : 'POST',
              url: "http://localhost:3333/api/articles",
              body:{
                  "body":"test1020102",
                  "description" :"We are here",
                  "title":"Hello there",
                  "tagList":["abcdedd"]
                      
                  
              },
              headers: {
                     "Authorization" : authorization,
            
                    }
          })
               .then((response) => {
                      expect(response.body).to.have.property('statusCode', 200);
                      expect(response.body.message).to.equal("Created successfully");
                      expect(response.body.data.body).to.equal("test1020102");
                      slug = response.body.data.slug;
          })
    })


    it("Test Update Articles Request", () => {
        const authorization = 'Bearer ' + token;
          cy.request({
              method : 'PUT',
              url: `http://localhost:3333/api/articles/${slug}`,
              body:{
                  "body":"test0102",
                  "description" :"We are here",
                  "title":"Hello there",
                  "tagList":["abcdedd"]
                      
                  
              },
              headers: {
                     "Authorization" : authorization,
              }
          })
               .then((response) => {
                      expect(response.body).to.have.property('statusCode', 200);
                      expect(response.body.message).to.equal("Updated successfully"); // Check the success message.
                      expect(response.body.data.body).to.equal("test0102"); // Check the data is upodated or not.
          })
    })

    it("Test Delete Articles Request", () => {
        const authorization = 'Bearer ' + token;
          cy.request({
              method : 'DELETE',
              url: `http://localhost:3333/api/articles/${slug}`,
              headers: {
                     "Authorization" : authorization,
              }
          })
               .then((response) => {
                      expect(response.body).to.have.property('statusCode', 200);
                      expect(response.body.message).to.equal("Deleted successfully"); // Check the success message.
                      expect(response.body.data).to.equal(null); // Check the data is deleted or not.
          })
    })



 
})