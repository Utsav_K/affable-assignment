describe('Conduit Login', () => {
    beforeEach(() => {

      cy.exec('npm run seed')
      cy.visit('http://localhost:4200')
      // we are not logged in
    })
    const invalid_email = "invalid@email.com";
    const invalid_password =" wrongpassword";
    const valid_email = "test@email.com";
    const valid_password = "123456";
  
    it('does not work with wrong credentials', () => {
      cy.contains('a.nav-link', 'Sign in').click()
  
      cy.get('[name=email]').type(invalid_email)
      cy.get('[name=password]').type(invalid_password)
      cy.get('button[type="submit"]').click()
  
      // error message is shown and we remain on the login page
      cy.url().should('contain', '/login')
    })
  
    it('logs in', () => {
      cy.contains('a.nav-link', 'Sign in').click()
  
      cy.get('[name=email]').type(valid_email)
      cy.get('[name=password]').type(valid_password)
      cy.get('button[type="submit"]').click()
  
      // when we are logged in, the navbar will contain profile navmenu
      cy.get(':nth-child(6) > .nav-link').should('have.attr', 'href')

      // when we are logged in, the navbar will contain settings navmenu
     cy.get(':nth-child(3) > .nav-link').should('have.attr', 'href').and('contains', "#/settings")

     // when we are logged in, the Your feed tab will be active tab
     cy.contains('a.nav-link', 'Your Feed').should('have.class', 'active')

      // url is changes from"/login"
      cy.url().should('not.contain', '/login')
    })
    afterEach( ( )=> {
      cy.exec('npm run revert')
    })
  })
  