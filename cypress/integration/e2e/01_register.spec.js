describe('Conduit Register', () => {
    beforeEach(() => {
      cy.exec('npm run revert')
        cy.visit('http://localhost:4200')
    })
    
 
    it('registers new user', () => {
        const username = 'user2'
        const email = 'user2@email.com'
        const password = '123456'
        cy.contains('a.nav-link', 'Sign up').click()
    
        cy.url().should('eq', 'http://localhost:4200/#/register')
        cy.get('[name=username]').type(username)
        cy.get('[name=email]').type(email)
        cy.get('[name=password]').type(password)
        cy.get('form').submit()
    
        cy.location('pathname').should('equal', '/')
        cy.get(':nth-child(6) > .nav-link')
     .should('have.attr', 'href').and('include', username)
      })

      afterEach(() => {
        cy.exec('npm run revert')

      })
    
    })
  