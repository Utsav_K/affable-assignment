

describe('Profile', () => {

  before(() => {
    cy.exec('npm run seed')
    cy.visit("http://localhost:4200")
    cy.login("test@email.com","123456")
  })
    const updatedusername="test1";
    const updatedBio = "Hi this is me";
    
    
    it('displays my profile', () => {
        cy.wait(5000)
        cy.get(':nth-child(6) > .nav-link').click()
        //the username would be displayed at the profile 
        cy.get(':nth-child(6) > .nav-link').invoke('text').then((text ) =>
      {
          expect(text).to.eql(" test ")
      })
      
    })
  
  
    it('can update my profile', () => {
      cy.wait(2000)
      cy.get('.col-xs-12 > .btn').click()
      cy.url().should('eq', 'http://localhost:4200/#/settings')

      cy.get('[name="image"]').clear()
      cy.get('[name="image"]').type("https://i.picsum.photos/id/237/200/300.jpg?hmac=TmmQSbShHz9CdQm0NkEjx1Dyh_Y984R9LpNrpvH2D_U")
      cy.get('[name="username"]').clear()
      cy.get('[name="username"]').type("test1")
      cy.get('[name="bio"]').clear()
      cy.get('[name="bio"]').type("Hi this is me")
      cy.get('form').submit()
  
      // updated name should be displayed in profile name.
      cy.get(':nth-child(6) > .nav-link').invoke('text').then((text ) =>
      {
          expect(text).to.eql(" "+updatedusername+" ")
      })
       // updated name should be displayed in username field.
      cy.get('[name="username"]').invoke('val').then((val ) =>
      {
          cy.log(val)
          expect(val).to.eql(updatedusername)
      })
      // updated bio should be displayed in bio field.
      cy.get('[name="bio"]').invoke('val').then((val1 ) =>
      {
          cy.log(val1)
          expect(val1).to.eql(updatedBio)
      })
    })
    
    it('logs out using button', () => {
        cy.get('.btn-outline-danger').click()
        cy.wait(2000)
        //SignIn button should be visible
        cy.get(':nth-child(3) > .nav-link').should('be.visible')
      })
      after(()=>{
        cy.exec('npm run revert')
      })
  })