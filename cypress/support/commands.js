

Cypress.Commands.add('login', (email,password) => {
    cy.contains('a.nav-link', 'Sign in').click()
    cy.get('[name=email]').type(email)
    cy.get('[name=password]').type(password)
    cy.get('button[type="submit"]').click()
})