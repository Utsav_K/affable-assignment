import {MigrationInterface, QueryRunner} from "typeorm";

export class logindata1638776931408 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<void> {

       await queryRunner.query(`INSERT INTO user (id, createdAt, email, username, password,image,bio) VALUES ('34da1127-7881-4ad5-9687-c6d484e912f3', '2021-12-06 13:49:25', 'test@email.com', 'test', '$2b$10$Z/aUx..hg2KVMSFgf3wqAeCQIDJFgnRoASlIpW6K6RqC5r8Gm4WTW','https://i.picsum.photos/id/237/200/300.jpg?hmac=TmmQSbShHz9CdQm0NkEjx1Dyh_Y984R9LpNrpvH2D_U','Hi this is me')`)
       
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DELETE FROM user where id='34da1127-7881-4ad5-9687-c6d484e912f3'`)
    }
}
