import { Component } from '@angular/core';

@Component({
  selector: 'realworld-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor() {}
  getData() {
    return { message: 'Welcome to Conduit!' };
  }
}
